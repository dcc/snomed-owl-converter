# snomed-owl-converter

This repository contains the Snomed OWL Toolkit in its binary form. It is compiled from  https://github.com/IHTSDO/snomed-owl-toolkit.


## License
Licensed under the Apache License, Version 2.0.